El smart-contract hace lo siguiente:

Los padres le dan algo de dinero a sus hijos y desean que solo puedan gastarse en algunos lugares/establecimientos conocidos y permitidos. Como ejemplo, a los padres les gustaría que sus hijos gasten el dinero en la escuela pero no les gustaría que sus hijos gasten el dinero en la tienda de dulces. El flujo sería así: Nick es un niño y tiene una aplicación de billetera en su teléfono móvil.

- Nick va a un lugar para comprar algo y selecciona algunos artículos.
- Ahora, necesita la dirección (billetera) del lugar y el monto a pagar.
- Nick abre su aplicación de billetera, ingresa la dirección del lugar y la cantidad.
Nick esta intentado pagar: si el lugar es un lugar permitido, se emitirá el pago. De lo contrario, la aplicación informará que no está permitido comprar allí.

# Install app

npm install -g truffle